import logo from './logo.svg';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Cardcomponent from './components/Cardcomponent'
import { Container,Row,Col } from 'react-bootstrap';
import NavbarCompo from './components/NavbarCompo';
import React, { Component } from 'react'


export default class App extends Component {
  constructor(props){
    super(props);
    this.state={
      data:[
        { cardNo:1,
          img:'img/angkor.jpg',
         title: 'Angkor Wat',
         description: 'In SiemReap, Cambodia',
        
        },
        {
          cardNo:2,
          img:'img/bayon.jpg',
          title: 'Bayon temple',
          description: 'In SiemReap, Cambodia'
        },
        {
          cardNo:3,
          img: 'img/preykuk.jpg',
          title: 'Prey Kuk temple',
          description: 'In Kampong Thom, Cambodia'
        }
      ]
    }
  }
  removeCard(cardNo){
    this.setState({data: this.state.data.filter(obj => obj.cardNo!=cardNo)});
  }
  render() {
    
    return (
      <>
      
      <Container>
      <NavbarCompo/>
        <Row>
          {
            this.state.data.map((obj)=>
            <Col key={obj.cardNo} md={3} sm={6}>
            <Cardcomponent removeCard={this.removeCard.bind(this)} data={obj} />
          </Col>
            )
          }
        </Row>
      </Container>
      </>
    )
  }
}



