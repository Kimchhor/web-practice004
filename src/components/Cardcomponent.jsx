
import {Card,Button} from 'react-bootstrap'
import React, { Component } from 'react'
import { Container,Row,Col } from 'react-bootstrap';
import '../App.css'
//import React from 'react'



export default class Cardcomponent extends Component {
  constructor(props){super(props);}
  
  render() {
    let {cardNo,img,title,description} = this.props.data
    return (
      <Card  >
        <Card.Img variant="top" src={img} />
          <Card.Body>
              <Card.Title>{title}</Card.Title>
              <Card.Text>
                        {description}
              </Card.Text>
              <Button onClick={() => this.props.removeCard(cardNo)} variant="primary">Delete</Button>
          </Card.Body>
      </Card>
    )
  }
}


